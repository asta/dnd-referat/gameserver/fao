FROM alpine:latest

COPY . /app
WORKDIR /app

RUN apk update && apk add npm

RUN npm install 

#RUN npm run watch-p
#RUN npm run build-s && npm run start
RUN npm run build

EXPOSE 3000

ENTRYPOINT npm run start
#ENTRYPOINT npm run build-s && npm run start
